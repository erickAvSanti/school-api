var mongoose = require('mongoose');
var _url = 'mongodb://localhost:27017/school-db';

mongoose.connect(_url).catch(function(err){ 
	console.log("error",err);
		process.exit(1);
}); 
mongoose.connection.on('disconnected',function(){
	console.log("db disconnected => "+_url); 
		process.exit(1);
});
mongoose.connection.on('connected',function(){
	console.log("db connected => "+_url);
	processUsers();  
});
mongoose.connection.on('connecting',function(){
	console.log("db connecting => "+_url);
		process.exit(1);
});
mongoose.connection.on('disconnecting',function(){
	console.log("db disconnecting => "+_url);
		process.exit(1);
});
 
function processUsers(){
	var User = require('./user/User');
	User.find({roles_names:{$in:['ADMIN','ADMINISTRADOR','ADMINISTRATOR']}},'_id name email',function(err,users){
		if(err){
			console.log(err);
		}
		if(users){
			console.log(users);
		}
		process.exit(1);
	}); 
}  
 
