var mongoose = require('mongoose');
var _props = {  
    _id: mongoose.Schema.Types.ObjectId,
    name: {type:String,required:true},
    last_name: {type:String,required:true},
    mother_last_name: {type:String,required:false}, 
    phone1: {type:String,required:false}, 
    phone2: {type:String,required:false}, 
    email1: {type:String,required:false}, 
    email2: {type:String,required:false}, 
    email1_active: {type:Boolean,required:false}, 
    email2_active: {type:Boolean,required:false}, 
    sex: {type:String,required:false}, 
    type: {type:String,required:false},  
    doc_type: {type:String,required:false},  
    description: {type:String,required:false},  
    document: {type:String,required:false,index:true},  
    from:[{ type: mongoose.Schema.Types.ObjectId, ref: 'Student' }],
};
var StudentRelativeSchema = new mongoose.Schema(_props,
{ 
    timestamps: { 
        createdAt: 'created_at',
        updatedAt:'updated_at' 
    }
});
StudentRelativeSchema.statics.props = _props;
StudentRelativeSchema.statics.makeFrom = function(obj){
    var _data = {from:[]};
    for(var idx in _props){ 
        if(idx in obj){
            var _prop = obj[idx];
            if(typeof _prop != "object" && _prop!=''){
                _data[idx] = _prop;
            }
        } 
    }
    return _data;
};
StudentRelativeSchema.statics.fillFrom = function(model,obj){ 
    for(var idx in _props){ 
        if(idx in obj){
            var _prop = obj[idx];
            if(typeof _prop != "object" && _prop!=''){
                model[idx] = _prop;
            }
        } 
    } 
};
StudentRelativeSchema.pre('save', function(next) {
    next();
}); 
mongoose.model('StudentRelative', StudentRelativeSchema);

module.exports = mongoose.model('StudentRelative');