var mongoose = require('mongoose');
var _url = 'mongodb://localhost:27017/school-db';

mongoose.connect(_url).catch(function(err){ 
	console.log("error",err);
		process.exit(1);
}); 
mongoose.connection.on('disconnected',function(){
	console.log("db disconnected => "+_url); 
		process.exit(1);
});
mongoose.connection.on('connected',function(){
	console.log("db connected => "+_url);
	processUsers();  
});
mongoose.connection.on('connecting',function(){
	console.log("db connecting => "+_url);
		process.exit(1);
});
mongoose.connection.on('disconnecting',function(){
	console.log("db disconnecting => "+_url);
		process.exit(1);
});
 
function processUsers(){
	var User = require('./user/User');
	User.find({},'_id tokens',function(err,users){
		if(err){
			console.log(err);
		}
		if(users && Array.isArray(users) && users.length>0){
			for(var idx in users){
				var user = users[idx]; 
				parseUser(user);

			}
		}else{
			process.exit(1);
		}
	}); 
} 
function parseUser(user){ 
	if(user && user.tokens && Array.isArray(user.tokens)){
		var _tokens = user.tokens.slice(); 
		for(var idx in _tokens){
			var token_id = _tokens[idx]; 
			if(typeof idx !=="number" && typeof idx !=="string")continue;
			if(typeof idx=="string" && !/[\d]+/.test(idx))continue;  
			parseTokenID(user,token_id); 
		}
	}
}
function parseTokenID(user,token_id){
	var UserToken = require('./user/UserToken');
	UserToken.findById(token_id,'_id expire',function(err,token){ 
		if(token){ 
			if(token.expire instanceof Date){
				if(token.expire < Date.now()){
					token.remove();
					user.tokens.pull(token._id);
					user.save(function(a,b){});
				}
			}
		}else{
			console.log("not found",token_id);
			user.tokens.pull(token_id);
			user.save(function(a,b){});
		}
	});
}
 
