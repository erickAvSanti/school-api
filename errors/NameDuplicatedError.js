class NameDuplicatedError extends Error {
  constructor(opts) {
  	if(typeof opts=='string'){
  		super(opts); 
  	}
  	if(typeof opts=='object'){
  		super(opts.message);
  		this.errmsg=opts.attr + " exists"; 
  		this.code=45000; 
  	}
    
    this.name = this.constructor.name; 
    Error.captureStackTrace(this, this.constructor);
  }
}
module.exports = NameDuplicatedError;