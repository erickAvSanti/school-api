var mongoose = require('mongoose');  
var UserTokenSchema = new mongoose.Schema({  
	_id: mongoose.Schema.Types.ObjectId,
  	token: {type:String,required:true,index:true,unique:true}, 
  	expire: {
  		type:Date,
  		required:true,
  		default:function(){
        //return new Date(Date.now()+5*60*1000);
  			return new Date(Date.now()+24*60*60*1000);
  		}
  	},
    ua:{
      type:String,
    },
  	created_by: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }, 
},
{ 
    timestamps: { 
        createdAt: 'created_at',
        updatedAt:'updated_at' 
    }
});
mongoose.model('UserToken', UserTokenSchema);

module.exports = mongoose.model('UserToken');