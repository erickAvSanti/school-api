var mongoose = require('mongoose');
var _url = 'mongodb://localhost:27017/school-db';
var make_connection = function(){
	//console.log("trying connection => "+_url);
	mongoose.connect(_url).catch(function(err){
		//console.log("cant connect => "+_url);
		setTimeout(make_connection,1000);
	}); 
};
mongoose.connection.on('disconnected',function(){
	//console.log("db disconnected => "+_url);
	setTimeout(make_connection,1000);
});
mongoose.connection.on('connected',function(){
	//console.log("db connected => "+_url);
});
mongoose.connection.on('connecting',function(){
	//console.log("db connecting => "+_url);
});
mongoose.connection.on('disconnecting',function(){
	//console.log("db disconnecting => "+_url);
});
make_connection();
module.exports = mongoose;