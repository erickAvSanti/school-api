var obj = {};
obj.is_numeric=function(str){
	return typeof str=="number" || (typeof str == "string" && /^[\d]+$/.test(str));
}
module.exports = obj;