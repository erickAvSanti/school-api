var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var crypto = require('crypto');
var mongoose = require('mongoose');
var email_validator = require("email-validator");
var random = require("random-js")();
var UserTokenController = require('../user/UserTokenController');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    res.header("Access-Control-Allow-Headers", "X-API-KEY, X-ACCESS-TOKEN, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    //res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Content-Type', 'application/json');

    for(var idx in req.body){
        if(typeof req.body[idx] =="string"){
            req.body[idx] = req.body[idx].trim();
        }
    }
    for(var idx in req.params){
        if(typeof req.params[idx] == "string"){
            req.params[idx] = req.params[idx].trim();
        }
    }   
    next();
});

var _verifyPermission = function(req,res,next){
    next();
    return;
    var method =req['method'];
    if(method=='GET'){

    }
    res.status(500).send({message:'cant-verify-permission'});
}

var _permission = function(req,res,next){   
    UserTokenController.verifyHeaderToken(req,res,function(pass,usertoken){
        if(pass){ 
            _verifyPermission(req,res,next);
        }else{
            res.end();
        }
    });
}


var Contact = require('./Contact');  
var Client = require('./Client');  

const TAG = "contact-client";
 

router.validateForm = function (req,res,options){
    if(!options)options={};
    if(options.name===undefined)options.name = true;
    if(options.last_name===undefined)options.last_name = true; 
    if(options.mother_last_name===undefined)options.mother_last_name = false; 
    if(options.phone1===undefined)options.phone1 = false; 
    if(options.phone2===undefined)options.phone2 = false;  
    if(options.doc_type===undefined)options.doc_type = false;  
    if(options.document===undefined)options.document = false;  
    if(options.sex===undefined)options.sex = false;  
    if(options.name){ 
        if(!req.body.name || req.body.name.trim()==''){
            return res.status(400).send({message:'name-undefined'});
        }
    }
    if(options.last_name){ 
        if(!req.body.last_name || req.body.last_name.trim()==''){
            return res.status(400).send({message:'lastname-undefined'});
        }
    } 
    if(options.mother_last_name){ 
        if(!req.body.mother_last_name || req.body.mother_last_name.trim()==''){
            return res.status(400).send({message:'mother-lastname-undefined'});
        }
    } 
    if(options.phone1){ 
        if(!req.body.phone1 || req.body.phone1.trim()==''){
            return res.status(400).send({message:'phone1-undefined'});
        }
    } 
    if(options.phone2){ 
        if(!req.body.phone2 || req.body.phone2.trim()==''){
            return res.status(400).send({message:'phone2-undefined'});
        }
    } 
    if(options.doc_type){ 
        if(!req.body.doc_type || req.body.doc_type.trim()==''){
            return res.status(400).send({message:'doctype-undefined'});
        }
    } 
    if(options.document){ 
        if(!req.body.document || req.body.document.trim()==''){
            return res.status(400).send({message:'document-undefined'});
        }
    }
    if(options.sex){ 
        var _sex = req.body.sex ? req.body.sex.trim() : '';
        if(
            _sex=='' || 
            (
                _sex!='MASCULINO' && 
                _sex!='FEMENINO'
            )
        ){
            return res.status(400).send({message:'sex-undefined'});
        }
    }
    return null;
}
 
router.post('/',_permission, function (req, res) {  
            var returned = router.validateForm(req,res);
            if(returned)return returned; 

            Client.findById(req.body.client_id, function (err1, client) {
                if (err1) return res.status(500).send({message:'client-not-found'});
                if (!client) return res.status(404).send({message:'client-not-found'});
                var _model = Contact.makeFrom(req.body);  
                _model._id  = new mongoose.Types.ObjectId();
                _model.from = client._id;
                Contact.create(
                _model, 
                function (err3, contact_created) {
                    if (err3){
                        console.log(err3);
                        var msg = 'error-contact-process';
                        if(err3){
                            switch(err3.code){
                                case 11000:
                                    msg = 'error-duplicate-key';
                                break;
                                default:
                            }
                        }
                        return res.status(503).send({message:msg});
                    }
                    if(contact_created){ 
                        client.contacts.push(contact_created._id);
                        client.save(function(err4,_client){});
                        res.status(200).send({message:TAG+'-created'});
                    }else{
                        res.status(403).send({message:TAG+'-not-created'});
                    } 
                });
            });  
});
 
router.get('/',_permission, function (req, res) {     
            Client.findById(req.query.client_id, function (err1, client) {
                if (err1) return res.status(500).send({message:'client-not-found'});
                if (!client) return res.status(404).send({message:'client-not-found'});
                Contact.find({from:client._id}, function (err, contacts) {
                    if (err) return res.status(500).send({message:'server-error'});
                    res.status(200).send(contacts);
                });
            });  
}); 
router.delete('/',_permission, function (req, res) {  
            Client.findById(req.query.client_id, function (err1, client) {
                if (err1) return res.status(500).send({message:'client-not-found',client_id:req.params.client_id});
                if (!client) return res.status(404).send({message:'client-not-found',client_id:req.params.client_id});
                Contact.findOne({
                    _id:mongoose.Types.ObjectId(req.query.id),
                    from:mongoose.Types.ObjectId(req.query.client_id),
                }, function (err, contact) {
                    if (err) return res.status(500).send({message:'server-error'}); 
                    if(contact){
                        contact.remove(function(err2,_contact){
                            if(!err2){ 
                                client.contacts.pull(contact._id);
                                client.save(function(a,b){});
                            }
                        });
                    }
                    res.status(200).send({message:TAG+'-deleted',id:req.query.id});
                });
            });     
});
 
router.put('/',_permission, function (req, res) { 
            var returned = router.validateForm(req,res);
            if(returned)return returned;
            Client.findById(req.body.client_id, function (err1, client) {
                if (err1) return res.status(500).send({message:'client-not-found'});
                if (!client) return res.status(404).send({message:'client-not-found'}); 
                Contact.findOne({
                    _id:mongoose.Types.ObjectId(req.body._id),
                    from:mongoose.Types.ObjectId(req.body.client_id),
                }, function(err1, contact) {
                    if (err1) return res.status(503).send({message:'server-error-0x0'});
                    if(!contact){
                        return res.status(404).send({message:TAG+'-not-found',id:req.body._id});
                    }else{
                        Contact.fillFrom(contact,req.body); 
                        contact.save(function(err2,_contact){
                            if(err2)return res.status(503).send({message:'server-error-0x1'});
                            if(_contact)return res.status(200).send({message:TAG+'-updated'}); 
                            return res.status(503).send({message:'server-error-0x2'}); 
                        });  
                    } 
                }); 
            });   
});



module.exports = router;