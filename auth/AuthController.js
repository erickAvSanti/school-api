// AuthController.js
var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());
var User = require('../user/User');
var UserController = require('../user/UserController');
var UserToken = require('../user/UserToken');
var UserTokenController = require('../user/UserTokenController');

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../config');


router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    res.header("Access-Control-Allow-Headers", "X-API-KEY, X-ACCESS-TOKEN, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    //res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Content-Type', 'application/json');
    
    for(var idx in req.body){
        if(typeof req.body[idx] =="string"){
            req.body[idx] = req.body[idx].trim();
        }
    }
    for(var idx in req.params){
        if(typeof req.params[idx] == "string"){
            req.params[idx] = req.params[idx].trim();
        }
    }

    next();
}); 

router.get('/me', function(req, res) {
  UserTokenController.verifyHeaderToken(req,res,function(ret,usertoken){
    if(ret){ 
      User.findOne({token:token},function(err,usertoken){
        if(err)return res.status(401).send({ auth: false, message: 'no-token-provided' });
        return res.status(200).send(usertoken);
      });
    }
  }); 
});
router.post('/login', function(req, res) {
  var returned = UserController.validateForm(req,res,{name:false});
  if(returned)return returned;
  User.findOne({ email: req.body.email }, function (err, user) {
    if (err)return res.status(500).send({success:false,message:'server-error'});
    if (!user)return res.status(404).send({success:false,message:'email-not-found'});
    user.comparePassword(req.body.password,function(err4,match){
      if(match){ 
        var token = req.headers['x-access-token'];
        if(!token){
          UserTokenController.newToken(user,req,function(err2,_newtoken){
            if (err2)return res.status(500).send({success:false,message:'server-error'});
            res.status(200).send({success:true,token:_newtoken.token,expire:_newtoken.expire}); 
          }); 
        }else{
          UserTokenController.validate(token,function(err5,usertoken,isValid,timeDiff){
            if(isValid)return res.status(200).send({success:true,token:usertoken.token,expire:usertoken.expire});
            if(usertoken)usertoken.remove();
            UserTokenController.newToken(user,req,function(err2,_newtoken){
              if (err2)return res.status(500).send({success:false,message:'server-error'});
              if (!_newtoken)return res.status(500).send({success:false,message:'cant-create-token'});
              res.status(200).send({success:true,token:_newtoken.token}); 
            });
          }); 
        } 
      }else{
        res.status(400).send({message:'invalid-password'});
      }
    }); 
  });
});
router.get('/verify-token', function(req, res) {
  UserTokenController.verifyHeaderToken(req,res,function(ret){
    if(ret)return res.status(200).send({ success:true });
  });
  /*
  var token = req.headers['x-access-token'];
  if(!token)return res.status(406).send({success:false,message:'no-token-provided'});
  UserTokenController.validate(token,function(err5,usertoken,isValid,timeDiff){
    if(!isValid)return res.status(404).send({success:false,message:'invalid-token',token:token}); 
    return res.status(200).send({success:true,message:'valid-token'});
  });
  */ 
});

router.get('/logout', function(req, res) {

  UserTokenController.verifyHeaderToken(req,res,function(ret,usertoken){
    if(ret){
      if(usertoken){
        UserTokenController.removeToken(req,res,usertoken.token,function(removed,_usertoken){ 
        }); 
      }
    }
    res.status(200).send({ success:true }); 
  });
  /*
  var token = req.headers['x-access-token'];
  if(!token)return res.status(406).send({success:false,message:'no-token-provided'});
  UserTokenController.validate(token,function(err5,usertoken,isValid,timeDiff){
    if(!isValid)return res.status(404).send({success:true,message:'invalid-token',token:token});
    UserTokenController.removeToken(req,res,token,function(removed,usertoken){
      if(!removed)return res.status(404).send({success:true,message:'token-not-found',token:token});
      res.status(200).send({ success:true });
    }); 
  });
  */ 
});

 

// add this to the bottom of AuthController.js
module.exports = router;