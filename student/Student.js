var mongoose = require('mongoose');
var moment = require('moment');
var _props = {  
    _id: mongoose.Schema.Types.ObjectId,
    name: {type:String,required:true,index:true},
    last_name: {type:String,required:false,index:true},
    mother_last_name: {type:String,required:false,index:true}, 
    doc_type: {type:String,required:false},  
    document: {type:String,required:false,index:true}, 
    description: {type:String,required:false}, 
    birthday: {type:Date,required:false},  
    relatives:[{ type: mongoose.Schema.Types.ObjectId, ref: 'StudentRelative' }],
};
var StudentSchema = new mongoose.Schema(_props,
{ 
    timestamps: { 
        createdAt: 'created_at',
        updatedAt:'updated_at' 
    }
});
StudentSchema.pre('save', function(next) {
    next();
}); 
StudentSchema.statics.props = _props;
var _dateFormat = function(str){
    return /^\d{2}\/\d{2}\/\d{4}$/.test(str);
};
StudentSchema.statics.isDateFormat = _dateFormat;
StudentSchema.statics.makeFrom = function(obj){
    var _data = {relatives:[]};
    for(var idx in _props){ 
        if(idx in obj){
            var _prop = obj[idx]; 
            if(idx=='birthday'){
                if(_prop!='' && _dateFormat(_prop)){
                    _data[idx] = new Date(moment(_prop,"DD/MM/YYYY").format("YYYY-MM-DD"));
                }else{
                    _data[idx] = null;
                }
            }else if(typeof _prop != "object" && _prop!=''){
                _data[idx] = _prop;
            }else if(Array.isArray(_prop)){  
                if(_prop.length>0){
                    _data[idx] = _prop.slice(); 
                } 
            }
        } 
    }
    return _data;
};
StudentSchema.statics.fillFrom = function(model,obj){ 
    for(var idx in _props){ 
        if(idx in obj){
            var _prop = obj[idx];
            if(idx=='birthday'){
                if(_prop!='' && _dateFormat(_prop)){
                    model[idx] = new Date(moment(_prop,"DD/MM/YYYY").format("YYYY-MM-DD"));
                }else{
                    model[idx] = null;
                }
            }else if(typeof _prop != "object" && _prop!=''){
                model[idx] = _prop;
            }else if(idx=="relatives"){
                model[idx] = _prop.map(function(prop){
                    return prop;
                });
            }
        } 
    } 
};
mongoose.model('Student', StudentSchema);

module.exports = mongoose.model('Student');