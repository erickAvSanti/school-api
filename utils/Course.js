var mongoose = require('mongoose');
const NameDuplicatedError = require('../errors/NameDuplicatedError');
var _props = {  
    _id: mongoose.Schema.Types.ObjectId,
    name: {type:String,required:true,unique:true},  
    years:[Number],
};
var CourseSchema = new mongoose.Schema(_props,
{ 
    timestamps: { 
        createdAt: 'created_at',
        updatedAt:'updated_at' 
    } 
});
CourseSchema.statics.props = _props;
CourseSchema.statics.makeFrom = function(obj){
    var _data = {};
    for(var idx in _props){ 
        if(idx in obj){
            var _prop = obj[idx];
            if(typeof _prop != "object" && _prop!=''){
                _data[idx] = _prop;
            }else{
                if(Array.isArray(_prop)){
                    _data[idx] = _prop;
                }else{ 
                    if(_prop!=null){ 
                        console.log("cant pass");
                        console.log(typeof _prop);
                        console.log(idx,_prop);
                    }
                }
            }
        } 
    }
    return _data;
};
CourseSchema.statics.fillFrom = function(model,obj){ 
    for(var idx in _props){ 
        if(idx in obj){
            var _prop = obj[idx];
            if(typeof _prop != "object" && _prop!=''){
                model[idx] = _prop;
            }else{
                if(Array.isArray(_prop)){
                    model[idx] = _prop;
                }else{ 
                    if(_prop!=null){ 
                        console.log("cant pass");
                        console.log(typeof _prop);
                        console.log(idx,_prop);
                    }
                }
            }
        } 
    } 
}; 
const Course = mongoose.model('Course', CourseSchema);
CourseSchema.pre('save', function(next) { 
    var _this = this; 
    var re = new RegExp(`^${this.name}$`,'i');
    Course.find({name:re},function(err,docs){ 
        if(docs){
            if(docs.length>0){
                var find = false;
                for(var idx in docs){
                    var doc = docs[idx]; 
                    if(doc._id.toString() == _this._id.toString()){
                        find = true;
                        break;
                    }
                }
                if(find){
                    return next();
                } 
                next(new NameDuplicatedError({attr:_this.name}));
            }else{
                next();
            }
        }
    });
    
}); 

module.exports = Course;