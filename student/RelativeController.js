var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var crypto = require('crypto');
var mongoose = require('mongoose');
var email_validator = require("email-validator");
var random = require("random-js")();
var UserTokenController = require('../user/UserTokenController');
var StudentController   = require('./StudentController');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    res.header("Access-Control-Allow-Headers", "X-API-KEY, X-ACCESS-TOKEN, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    //res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Content-Type', 'application/json');

    for(var idx in req.body){
        if(typeof req.body[idx] =="string"){
            req.body[idx] = req.body[idx].trim();
        }
    }
    for(var idx in req.params){
        if(typeof req.params[idx] == "string"){
            req.params[idx] = req.params[idx].trim();
        }
    }   
    next();
});

var _verifyPermission = function(req,res,next){
    next();
    return;
    var method =req['method'];
    if(method=='GET'){

    }
    res.status(500).send({message:'cant-verify-permission'});
}

var _permission = function(req,res,next){   
    UserTokenController.verifyHeaderToken(req,res,function(pass,usertoken){
        if(pass){ 
            _verifyPermission(req,res,next);
        }else{
            res.end();
        }
    });
}


var Relative = require('./Relative');  
var Student = require('./Student');  

const TAG = "realtive-student";
 

router.validateForm = function (req,res,options){
    if(!options)options={};
    if(options.name===undefined)options.name = true;
    if(options.last_name===undefined)options.last_name = true; 
    if(options.mother_last_name===undefined)options.mother_last_name = false; 
    if(options.phone1===undefined)options.phone1 = false; 
    if(options.phone2===undefined)options.phone2 = false;  
    if(options.type===undefined)options.type = false;  
    if(options.doc_type===undefined)options.doc_type = false;  
    if(options.document===undefined)options.document = false;  
    if(options.sex===undefined)options.sex = false;  
    if(options.name){ 
        if(!req.body.name || req.body.name.trim()==''){
            return res.status(400).send({message:'name-undefined'});
        }
    }
    if(options.last_name){ 
        if(!req.body.last_name || req.body.last_name.trim()==''){
            return res.status(400).send({message:'lastname-undefined'});
        }
    } 
    if(options.mother_last_name){ 
        if(!req.body.mother_last_name || req.body.mother_last_name.trim()==''){
            return res.status(400).send({message:'mother-lastname-undefined'});
        }
    } 
    if(options.phone1){ 
        if(!req.body.phone1 || req.body.phone1.trim()==''){
            return res.status(400).send({message:'phone1-undefined'});
        }
    } 
    if(options.phone2){ 
        if(!req.body.phone2 || req.body.phone2.trim()==''){
            return res.status(400).send({message:'phone2-undefined'});
        }
    } 
    if(options.doc_type){ 
        if(!req.body.doc_type || req.body.doc_type.trim()==''){
            return res.status(400).send({message:'doctype-undefined'});
        }
    } 
    if(options.type){ 
        if(!req.body.type || req.body.type.trim()==''){
            return res.status(400).send({message:'type-undefined'});
        }
    } 
    if(options.document){ 
        if(!req.body.document || req.body.document.trim()==''){
            return res.status(400).send({message:'document-undefined'});
        }
    }
    if(options.sex){ 
        var _sex = req.body.sex ? req.body.sex.trim() : '';
        if(
            _sex=='' || 
            (
                _sex!='MASCULINO' && 
                _sex!='FEMENINO'
            )
        ){
            return res.status(400).send({message:'sex-undefined'});
        }
    }
    return null;
}
 
router.post('/',_permission, function (req, res) {  
    var returned = router.validateForm(req,res);
    if(returned)return returned; 

    var _model = Relative.makeFrom(req.body);  
    _model._id  = new mongoose.Types.ObjectId(); 
    //_model.from.push(student._id);
    if(req.body.student_id){
        _model.from.push(new mongoose.Types.ObjectId(req.body.student_id));
    }
    Relative.create(
    _model, 
    function (err3, relative_created) {
        if (err3){
            console.log(err3);
            var msg = 'error-relative-process';
            if(err3){
                switch(err3.code){
                    case 11000:
                        msg = 'error-duplicate-key';
                    break;
                    case 45000:
                        msg = 'duplicated';
                    break;
                    default:
                }
            }
            return res.status(503).send({message:msg});
        }
        if(relative_created){
            res.status(200).send({message:TAG+'-created',reg:relative_created});
        }else{
            res.status(403).send({message:TAG+'-not-created'});
        }
        
    });  
});
 
router.get('/search',_permission, function (req, res) {  
    var _reg = `${req.query.search}`; 
    Relative.find({$or:[
            {
                name:{
                    $regex:_reg,
                    $options:'i',
                }
            },
            {
                last_name:{
                    $regex:_reg,
                    $options:'i',
                }
            },
            {
                mother_last_name:{
                    $regex:_reg,
                    $options:'i',
                }
            },
            {
                document:{
                    $regex:_reg,
                    $options:'i',
                }
            },
        ]}, function (err, relatives) {
        if (err){
            console.log(err);
            return res.status(500).send({message:'server-error'});
        }
        res.status(200).send(relatives);
    });
}); 
router.get('/',_permission, function (req, res) {   
    Relative.find({
        from:req.query.student_id,
    }, function (err, relatives) {
        if (err) return res.status(500).send({message:'server-error'});
        res.status(200).send(relatives);
    });  
}); 
router.delete('/',_permission, function (req, res) {  
    Relative.findById(req.query.id, function (err, relative) {
        if (err) return res.status(500).send({message:'server-error'}); 
        if(relative){
            if(Array.isArray(relative.from) && relative.from.length>0){ 
                relative.from.forEach(function(el){
                    StudentController.removeRelative(el,relative._id);
                });
            }else{
                //pass
            }
        }
        res.status(200).send({message:TAG+'-deleted',id:req.query.id});
    });    
});
 
router.put('/',_permission, function (req, res) { 
    var returned = router.validateForm(req,res);
    if(returned)return returned;
    Relative.findById(req.body._id, function(err1, relative) {
        if (err1) return res.status(503).send({message:'server-error-0x0'});
        if(!relative){
            return res.status(404).send({message:TAG+'-not-found',id:req.body._id});
        }else{
            Relative.fillFrom(relative,req.body); 
            relative.save(function(err2,_relative){
                if(err2)return res.status(503).send({message:'server-error-0x1'});
                if(_relative){
                    return res.status(200).send({message:TAG+'-updated',reg:_relative}); 
                }
                return res.status(503).send({message:'server-error-0x2'}); 
            });  
        } 
    });  
});

router.addStudentToRelative = function(student_id,relative_id){
    Relative.findById(relative_id,function(err,relative){
        if(relative){
            var find = false;
            relative.from.forEach(function(el){
                if(el.toString()==student_id)find=true;
            });
            if(!find){
                relative.from.push(student_id);
                relative.save(function(a,b){});
            }
        }
    });
};

router.removeStudent = function(relative_id,student_id){
    Relative.findById(relative_id,function(err,relative){
        if(relative){ 
            relative.from.pull(student_id);
            relative.save(function(a,b){});
        }
    });
};



module.exports = router;