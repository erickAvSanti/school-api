var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var crypto = require('crypto');
var mongoose = require('mongoose');
var moment = require('moment');
var email_validator = require("email-validator");
var random = require("random-js")();
var UserTokenController = require('../user/UserTokenController');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    res.header("Access-Control-Allow-Headers", "X-API-KEY, X-ACCESS-TOKEN, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    //res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Content-Type', 'application/json');

    for(var idx in req.body){
        if(typeof req.body[idx] =="string"){
            req.body[idx] = req.body[idx].trim();
        }
    }
    for(var idx in req.params){
        if(typeof req.params[idx] == "string"){
            req.params[idx] = req.params[idx].trim();
        }
    }   
    next();
});

var _verifyPermission = function(req,res,next){
    next();
    return;
    var method =req['method'];
    if(method=='GET'){

    }
    res.status(500).send({message:'cant-verify-permission'});
}

var _permission = function(req,res,next){   
    UserTokenController.verifyHeaderToken(req,res,function(pass,usertoken){
        if(pass){ 
            _verifyPermission(req,res,next);
        }else{
            res.end();
        }
    });
}


var Client = require('./Client');  

const TAG = "client";
 

router.validateForm = function (req,res,options){
    if(!options)options={};
    if(options.name===undefined)options.name = true;
    if(options.last_name===undefined)options.last_name = true; 
    if(options.doc_type===undefined)options.doc_type = true; 
    if(options.personality===undefined)options.personality = true; 
    if(options.document===undefined)options.document = true; 
    if(options.name){ 
        if(!req.body.name || req.body.name.trim()==''){
            return res.status(400).send({message:'name-undefined'});
        }
    }
    if(options.last_name){ 
        if(!req.body.last_name || req.body.last_name.trim()==''){
            return res.status(400).send({message:'lastname-undefined'});
        }
    }
    if(options.doc_type){ 
        if(!req.body.doc_type || req.body.doc_type.trim()==''){
            return res.status(400).send({message:'doctype-undefined'});
        }
    }
    if(options.personality){ 
        if(!req.body.personality || req.body.personality.trim()==''){
            return res.status(400).send({message:'personality-undefined'});
        }
    }
    if(options.document){ 
        if(!req.body.document || req.body.document.trim()==''){
            return res.status(400).send({message:'document-undefined'});
        }
    }
    return null;
}

// CREATES A NEW USER
router.post('/',_permission, function (req, res) {  
            var returned = router.validateForm(req,res);
            if(returned)return returned;

            Client.findOne({ document: req.body.document }, function(err1, client) {
                if (err1){
                    return res.status(503).send({message:'server-error'});
                }
                if(!client){
                    var _model = Client.makeFrom(req.body);  
                    _model._id  = new mongoose.Types.ObjectId(); 
                    Client.create(
                    _model, 
                    function (err3, client_created) {
                        if (err3){
                            console.log(err3);
                            return res.status(503).send({message:'error-process'});
                        }
                        if(client_created){ 
                            res.status(200).send({message:TAG+'-created'});
                        }else{
                            res.status(403).send({message:TAG+'-not-created'});
                        } 
                    });
                }else{
                    res.status(406).send({message:'document-already-exists'});
                }
            });  
});
 
router.get('/',_permission, function (req, res) {   
            Client.find({}, function (err, clients) {
                if (err) return res.status(500).send({message:'server-error'});
                res.status(200).send(clients);
            }); 
});
 
router.get('/:id',_permission, function (req, res) { 
            Client.findById(req.params.id, function (err1, client) {
                if (err1) return res.status(500).send({message:TAG+'-not-found'});
                if (!client) return res.status(404).send({message:TAG+'-not-found'});
                res.status(200).send({data:client});
            }); 
});
 
router.delete('/:id',_permission, function (req, res) { 
            Client.findById(req.params.id, function (err, client) {
                if (err) return res.status(500).send({message:'server-error'}); 
                if(client){
                    client.remove(function(err2,_client){});
                }
                res.status(200).send({message:TAG+'-deleted',id:req.params.id});
            }); 
});
 
router.put('/:id',_permission, function (req, res) {  
            var returned = router.validateForm(req,res);
            if(returned)return returned;

            Client.findById(req.params.id, function(err1, client) {
                if (err1) return res.status(503).send({message:'server-error-0x0'});
                if(!client){
                    return res.status(404).send({message:TAG+'-not-found',id:req.params.id});
                }else{ 
                    Client.fillFrom(client,req.body); 
                    client.save(function(err2,_client){
                        if(err2){
                            console.log(err2);
                            return res.status(503).send({message:'server-error-0x1'});
                        }
                        if(_client)return res.status(200).send({message:TAG+'-updated'}); 
                        return res.status(503).send({message:'server-error-0x2'}); 
                    });  
                } 
            });  
});



module.exports = router;