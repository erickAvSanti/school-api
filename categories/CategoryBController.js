var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser'); 
var mongoose = require('mongoose'); 
var UserTokenController = require('../user/UserTokenController');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    res.header("Access-Control-Allow-Headers", "X-API-KEY, X-ACCESS-TOKEN, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    //res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Content-Type', 'application/json');

    for(var idx in req.body){
        if(typeof req.body[idx] =="string"){
            var _reg = req.body[idx];
            req.body[idx] = _reg.replace(/ +/,' ').trim();
        }
    }
    for(var idx in req.params){
        if(typeof req.params[idx] == "string"){
            var _reg = req.params[idx];
            req.params[idx] = _reg.replace(/ +/,' ').trim();
        }
    }   
    next();
});

var _verifyPermission = function(req,res,next){
    next();
    return;
    var method =req['method'];
    if(method=='GET'){

    }
    res.status(500).send({message:'cant-verify-permission'});
}

var _permission = function(req,res,next){   
    UserTokenController.verifyHeaderToken(req,res,function(pass,usertoken){
        if(pass){ 
            _verifyPermission(req,res,next);
        }else{
            res.end();
        }
    });
}


var CategoryA = require('./CategoryA');  
var CategoryB = require('./CategoryB');  

const TAG = "category-b";
 

router.validateForm = function (req,res,options){
    if(!options)options={};
    if(options.name===undefined)options.name = true; 
    if(options.category_a_id===undefined)options.category_a_id = true; 
    if(options.name){ 
        if(!req.body.name || req.body.name.trim()==''){
            return res.status(400).send({message:'name-undefined'});
        }
    } 
    if(options.category_a_id){ 
        if(!req.body.category_a_id || req.body.category_a_id.trim()==''){
            return res.status(400).send({message:'category-a-id-undefined'});
        }
    } 
    return null;
}
 
router.post('/',_permission, function (req, res) {  
            var returned = router.validateForm(req,res);
            if(returned)return returned;
            CategoryA.findById(req.body.category_a_id, function(err1, reg) {
                if (err1){
                    return res.status(503).send({message:'server-error'});
                }
                if (!reg) return res.status(404).send({message:'category-a-not-found'}); 
                var _model = CategoryB.makeFrom(req.body);  
                _model._id  = new mongoose.Types.ObjectId(); 
                _model.from  = reg._id; 
                CategoryB.create(
                _model, 
                function (err3, reg_created) {
                    if (err3){ 
                        var _msg = 'error-process';
                        if(err3.code==11000 || err3.code==45000){
                            _msg='duplicated';
                        }
                        return res.status(503).send({message:_msg});
                    }
                    if(reg_created){ 
                        reg.categoryBs.push(reg_created._id);
                        reg.save(function(_a,_b){});
                        res.status(200).send({message:TAG+'-created'});
                    }else{
                        res.status(403).send({message:TAG+'-not-created'});
                    } 
                }); 
            });  
});
 
router.get('/',_permission, function (req, res) {   
            var category_a_id = req.query.category_a_id;
            CategoryB.find({from:category_a_id}, function (err, regs) {
                if (err) return res.status(500).send({message:'server-error'});
                res.status(200).send(regs);
            }); 
});
 
router.get('/:id',_permission, function (req, res) { 
            CategoryB.findById(req.params.id, function (err1, reg) {
                if (err1) return res.status(500).send({message:TAG+'-not-found'});
                if (!reg) return res.status(404).send({message:TAG+'-not-found'});
                res.status(200).send({data:reg});
            }); 
});
 
router.delete('/:id',_permission, function (req, res) { 
            CategoryB.findById(req.params.id, function (err, reg) {
                if (err) return res.status(500).send({message:'server-error'}); 
                if(reg){
                    if(reg.categoryCs.length<1){ 
                        reg.remove(function(err2,_reg){
                            if(!err2 && _reg){ 
                                CategoryA.findById(_reg.from,function(e,r){
                                    if(!e && r){ 
                                        r.categoryBs.pull(_reg._id);
                                        r.save(function(_a,_b){});
                                    }
                                });
                            }
                        });
                    }else{
                        return res.status(400).send({message:'has-category-c',id:req.params.id});
                    }
                }
                res.status(200).send({message:TAG+'-deleted',id:req.params.id});
            }); 
});
 
router.put('/:id',_permission, function (req, res) { 
            var returned = router.validateForm(req,res);
            if(returned)return returned;

            CategoryB.findById(req.params.id, function(err1, reg) {
                if (err1) return res.status(503).send({message:'server-error-0x0'});
                if(!reg){
                    return res.status(404).send({message:TAG+'-not-found',id:req.params.id});
                }else{ 
                    CategoryB.fillFrom(reg,req.body); 
                    reg.save(function(err2,_reg){
                        if(err2){
                            console.log(err2);
                            var _msg = 'server-error-0x1';
                            if(err2.code==11000 || err2.code==45000){
                                _msg='duplicated';
                            }
                            return res.status(503).send({message:_msg});
                        }
                        if(_reg)return res.status(200).send({message:TAG+'-updated'}); 
                        return res.status(503).send({message:'server-error-0x2'}); 
                    });  
                } 
            });  
});



module.exports = router;