var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var crypto = require('crypto');
var mongoose = require('mongoose');
var moment = require('moment');
var email_validator = require("email-validator");
var random = require("random-js")();
var UserTokenController = require('../user/UserTokenController');
var RelativeController = require('./RelativeController');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    res.header("Access-Control-Allow-Headers", "X-API-KEY, X-ACCESS-TOKEN, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    //res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Content-Type', 'application/json');

    for(var idx in req.body){
        if(typeof req.body[idx] =="string"){
            req.body[idx] = req.body[idx].trim();
        }
    }
    for(var idx in req.params){
        if(typeof req.params[idx] == "string"){
            req.params[idx] = req.params[idx].trim();
        }
    }   
    next();
});

var _verifyPermission = function(req,res,next){
    next();
    return;
    var method =req['method'];
    if(method=='GET'){

    }
    res.status(500).send({message:'cant-verify-permission'});
}

var _permission = function(req,res,next){   
    UserTokenController.verifyHeaderToken(req,res,function(pass,usertoken){
        if(pass){ 
            _verifyPermission(req,res,next);
        }else{
            res.end();
        }
    });
}


var Student = require('./Student');  

const TAG = "student";
 

router.validateForm = function (req,res,options){
    if(!options)options={};
    if(options.name===undefined)options.name = true;
    if(options.last_name===undefined)options.last_name = true; 
    if(options.doc_type===undefined)options.doc_type = false;  
    if(options.document===undefined)options.document = false; 
    if(options.name){ 
        if(!req.body.name || req.body.name.trim()==''){
            return res.status(400).send({message:'name-undefined'});
        }
    }
    if(options.last_name){ 
        if(!req.body.last_name || req.body.last_name.trim()==''){
            return res.status(400).send({message:'lastname-undefined'});
        }
    }
    if(options.doc_type){ 
        if(!req.body.doc_type || req.body.doc_type.trim()==''){
            return res.status(400).send({message:'doctype-undefined'});
        }
    } 
    if(options.document){ 
        if(!req.body.document || req.body.document.trim()==''){
            return res.status(400).send({message:'document-undefined'});
        }
    }
    return null;
}
 
router.post('/',_permission, function (req, res) {  
            var returned = router.validateForm(req,res);
            if(returned)return returned;

            var _model = Student.makeFrom(req.body);  
            _model._id  = new mongoose.Types.ObjectId();  
            Student.create(
            _model, 
            function (err3, student_created) {
                if (err3){
                    console.log(err3);
                    return res.status(503).send({message:'error-process'});
                }
                if(student_created){ 
                    if(
                        Array.isArray(req.body.relatives) && 
                        req.body.relatives.length>0
                    ){
                        req.body.relatives.forEach(function(el){ 
                            RelativeController.addStudentToRelative(
                                student_created._id.toString(),
                                el
                            );
                        });
                    }
                    res.status(200).send({message:TAG+'-created'});
                }else{
                    res.status(403).send({message:TAG+'-not-created'});
                } 
            });
});
 
router.get('/',_permission, function (req, res) {   
            Student.find({}, function (err, students) {
                if (err) return res.status(500).send({message:'server-error'});
                res.status(200).send(students);
            }); 
});
 
router.get('/:id',_permission, function (req, res) { 
            Student.findById(req.params.id, function (err1, student) {
                if (err1) return res.status(500).send({message:TAG+'-not-found'});
                if (!student) return res.status(404).send({message:TAG+'-not-found'});
                res.status(200).send({data:student});
            }); 
});
 
router.delete('/:id',_permission, function (req, res) { 
            Student.findById(req.params.id, function (err, student) {
                if (err) return res.status(500).send({message:'server-error'}); 
                if(student){
                    var _relatives = student.relatives;
                    student.remove(function(err2,_student){
                        if(!err2 && _student){
                            _relatives.forEach(function(rel){
                                RelativeController.removeStudent(rel,student._id);
                            });
                        }
                    });
                }
                res.status(200).send({message:TAG+'-deleted',id:req.params.id});
            }); 
});
 
router.put('/:id',_permission, function (req, res) {  
            var returned = router.validateForm(req,res);
            if(returned)return returned;

            Student.findById(req.params.id, function(err1, student) {
                if (err1) return res.status(503).send({message:'server-error-0x0'});
                if(!student){
                    return res.status(404).send({message:TAG+'-not-found',id:req.params.id});
                }else{ 
                    var _tmp_relatives = student.relatives;
                    Student.fillFrom(student,req.body); 
                    student.save(function(err2,_student){
                        if(err2){
                            console.log(err2);
                            return res.status(503).send({message:'server-error-0x1'});
                        }
                        if(_student){
                            if(_tmp_relatives.length>0){
                                _tmp_relatives.forEach(function(el_last){
                                    var _find = false;
                                    _student.relatives.forEach(function(el_new){
                                        if(el_new.toString()==el_last.toString()){
                                            _find = true;
                                        }
                                    });
                                    if(!_find){ 
                                        RelativeController.removeStudent(el_last,_student._id);
                                    }
                                });
                                _student.relatives.forEach(function(el_new){
                                    var _find = false;
                                    _tmp_relatives.forEach(function(el_last){
                                        if(el_new.toString()==el_last.toString()){
                                            _find = true;
                                        }
                                    });
                                    if(!_find){
                                        RelativeController.addStudentToRelative(_student._id,el_new);
                                    }
                                });
                            }else{
                                if(_student.relatives.length>0){
                                    _student.relatives.forEach(function(el_new){
                                        RelativeController.addStudentToRelative(_student._id,el_new);
                                    });
                                }
                            }
                            return res.status(200).send({message:TAG+'-updated'}); 
                        }
                        return res.status(503).send({message:'server-error-0x2'}); 
                    });  
                } 
            });  
});
router.removeRelative = function(student_id,relative_id){
    Student.findById(student_id,function(err,student){
        student.relatives.pull(relative_id);
        student.save(function(a,b){});
    });
} 
module.exports = router;