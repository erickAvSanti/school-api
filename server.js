var app = require('./app');
var port = 8285;

var server = app.listen(port, function() {
  console.log('Express server listening on port ' + port);
});
process.on('exit',function(err){
	console.log(err);
});
process.on('uncaughtException',function(err){
	console.log(err);
});
process.on('SIGTERM',function(err){
	console.log(err);
});